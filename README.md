# Solidabis koodihaaste 2021

## Tehtävänanto

Kesälomat lähestyvät ja monien katseet kääntyvät kohti kesämökkejä. Osalla nämä löytyvät lähempää, osalla taas matkustukseen kuluu pitkiäkin aikoja. Monesti tien päällä ollessa tuntuu siltä, että jos hieman vielä kiihdyttäisi, olisi perillä merkittävästi nopeammin… vai olisiko sittenkään? Ovatko voitetut minuutit kasvaneiden matkakustannusten arvoisia? Entä kuinka paljon matkustusajoneuvon tyyppi vaikuttaa tähän?

Tehtävänäsi on toteuttaa autoilumittari-sovellus. Sovelluksen tulee pystyä suorittamaan vertailu matka-ajan ja polttoaineen kulutuksen välillä kahden eri valitun nopeuden mukaan: käyttäjä ilmoittaa saman matkustettavan etäisyyden samalla kulkuneuvotyypillä eri nopeuksilla ja sovellus laskee miten paljon nopeammin käyttäjä on perillä ja kuinka paljon enemmän polttoainetta tähän kuluu. Etäisyyden sekä kulkuneuvotyypin tulee siis olla molemmissa samat. Sovelluksen tulee pystyä näyttämään web-käyttöliittymässä molemmista annetuista matkanopeuksista käytetty aika ja polttoaine, sekä näiden kahden ero.

Sovelluksessa tulee pystyä tarkastelemaan kolmen erilaisen auton tuloksia. Autojen bensankulutus kasvaa 1.009 kulmakertoimella. Eli jos auton nopeus kasvaa 1km/h, niin bensankulutus kasvaa 1.009 kertaiseksi. Eri autojen bensakulutus 1km/h nopeudella on seuraava:

```
Auto A: 3l / 100km
Auto B: 3.5l / 100km
Auto C: 4l / 100km
```

Toteutuksessa käytettävät teknologiat ovat vapaasti päätettävissäsi. Tehtävässä ei välttämättä ole tarpeen tehdä erillistä backend-toteutusta, mutta voit sen halutessasi tehdä. Tehtävässä ei saa käyttää mitään kolmannen osapuolen palvelua tai kirjastoa, mikä toteuttaa vaaditut vertailutoimenpiteet.

## Arviointi

Palautetussa työssä tulee olla ratkaistu kaikki annetut tehtävänannon osa-alueet. Arvioimme tuloksien oikeellisuutta, koodia, sen selkeyttä ja laatua sekä tehdyn ratkaisun ulkoasua.

## Palautus

Lähetä linkki projektisi Git-repositorioon 6.6.2021 mennessä sähköpostiosoitteeseen koodihaaste@solidabis.com. Voit myös asentaa web-käyttöliittymäsi pyörimään esimerkiksi Heroku-palveluun. Lähetä tällöin myös linkki sovellukseesi.

Lisää repositorioosi README.md tiedosto, jossa ilmenee vastausten tarkastelua helpottavat tiedot, eli:

- Mitä teknologioita olet käyttänyt ja millä käyttöjärjestelmällä
- Ohjeet miten ratkaisusi pystytetään ja käynnistetään
- Muutaman lauseen kuvaus tekemästäsi ratkaisusta
- Kerro samalla, haluatko osallistua vain kilpailuun ja arvontaan, vai haluatko Solidabiksen ottavan yhteyttä myös työtarjouksiin liittyen. Se ei tarkoita, että sinulle lähetettäisiin roskapostia, vaan nimensä mukaisesti esimerkiksi kutsu työhaastatteluun. Voit halutessasi osallistua koodihasteeseen myös ilman, että haluat ottaa palkintoa vastaan tai osallistua arvontaan.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3005](http://localhost:3005) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the app for production to the `build` folder.\
