export const config = {
  distance: {
    min: 1,
    max: 1500,
    default: 80,
  },
  speed: {
    min: 1,
    max: 280,
    default: {
      A: 80,
      B: 100,
    },
  },
  fuel: {
    price: 1.63,
  },
};
