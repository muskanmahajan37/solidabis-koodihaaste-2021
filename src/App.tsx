import { useState, useEffect } from 'react';
import './App.css';

import { ICar } from './interfaces/ICar';
import { calculateConsumption } from './helpers/consumption';
import { calculateTravelTime } from './helpers/travelTime';
import { config } from './App-config';

import { CarPicker } from './components/CarPicker';
import { DistancePicker } from './components/DistancePicker';
import { SpeedPicker } from './components/SpeedPicker';
import { Results } from './components/Results';

const App = () => {
  const [car, setCar] = useState<ICar | null>(null);
  const [distance, setDistance] = useState(config.distance.default);

  const [speedA, setSpeedA] = useState(config.speed.default.A);
  const [speedB, setSpeedB] = useState(config.speed.default.B);
  const [consumptionA, setConsumptionA] = useState(0);
  const [consumptionB, setConsumptionB] = useState(0);
  const [travelTimeA, setTravelTimeA] = useState(0);
  const [travelTimeB, setTravelTimeB] = useState(0);

  useEffect(() => {
    if (car && distance && speedA && speedB) {
      setConsumptionA(
        calculateConsumption(car.baseConsumption, speedA, distance)
      );
      setConsumptionB(
        calculateConsumption(car.baseConsumption, speedB, distance)
      );
      setTravelTimeA(calculateTravelTime(speedA, distance));
      setTravelTimeB(calculateTravelTime(speedB, distance));
    }
  }, [car, distance, speedA, speedB]);

  return (
    <>
      <div
        style={{
          width: '90%',
          margin: '0 auto',
          textAlign: 'center',
        }}
      >
        <h1>Autoilumittari</h1>

        <div>
          <h2>Millaisella autolla huristellaan?</h2>
          <CarPicker selectedCar={car} setCar={setCar} />

          <h2>Kuinkas pitkälle mennään?</h2>
          <DistancePicker distance={distance} setDistance={setDistance} />
        </div>
        <div
          style={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
          }}
        >
          <div>
            <h2>Normaali nopeus</h2>
            <SpeedPicker speed={speedA} setSpeed={setSpeedA} />
          </div>

          <div>
            <h2>Vertailunopeus</h2>
            <SpeedPicker speed={speedB} setSpeed={setSpeedB} />
          </div>
        </div>
        <div>
          <h2>Vertailun tulokset</h2>
          <Results
            consumptionA={consumptionA}
            consumptionB={consumptionB}
            travelTimeA={travelTimeA}
            travelTimeB={travelTimeB}
          />
        </div>

        <div className="footer">
          <a
            href="https://koodihaaste.solidabis.com/"
            title="Toteutettu Solidabiksen koodihaastessa 2021"
          >
            <img src="solidabis.png" alt="Solidabis logo" />
          </a>

          <a
            href="https://gitlab.com/damiqib/solidabis-koodihaaste-2021"
            title="Source code in GitLab"
          >
            <img src="gitlab-logo-gray-rgb.svg" alt="Source code in Gitlab" />
          </a>

          <p>2021 Janne Toikander</p>
        </div>
      </div>
    </>
  );
};

export default App;
