import { shallow } from 'enzyme';

import { CarPicker } from './CarPicker';

import { cars } from '../data/cars';

describe('CarPicker component', () => {
  it('should render Car components', () => {
    const wrapper = shallow(
      <CarPicker selectedCar={cars[0]} setCar={jest.fn()} />
    );

    expect(wrapper.find('Car').exists()).toEqual(true);
  });
});

export {};
