import React, { useEffect } from 'react';

import { ICar } from '../interfaces/ICar';
import { cars } from '../data/cars';

import { Car } from './Car';

type CarPickerProps = {
  selectedCar: ICar | null;
  setCar: React.Dispatch<React.SetStateAction<ICar | null>>;
};

const CarPicker = ({ selectedCar, setCar }: CarPickerProps): JSX.Element => {
  //By default, select the first car from the list
  useEffect(() => {
    if (selectedCar === null) {
      setCar(cars[0] as ICar);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // TODO: cars should be in a three-column grid - centered in their cells

  return (
    <>
      {cars.map((car) => (
        <Car
          car={car}
          key={car.name}
          selectedCar={selectedCar}
          setCar={setCar}
        />
      ))}
    </>
  );
};

export { CarPicker };
