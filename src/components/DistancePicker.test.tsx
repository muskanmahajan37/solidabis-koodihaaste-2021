import { shallow } from 'enzyme';

import { DistancePicker } from './DistancePicker';

describe('DistancePicker component', () => {
  it('should render an input with given distance', () => {
    const wrapper = shallow(
      <DistancePicker distance={100} setDistance={jest.fn()} />
    );

    expect(wrapper.find('span').text()).toContain('100');
    expect(wrapper.find('input.slider').exists()).toEqual(true);
  });

  it('should should call setDistance', () => {
    const mockRangeChange = jest.fn();

    const wrapper = shallow(
      <DistancePicker distance={100} setDistance={mockRangeChange} />
    );

    expect(mockRangeChange).not.toHaveBeenCalled();

    wrapper.find('input').simulate('change', { target: { value: 313 } });

    expect(mockRangeChange).toHaveBeenCalledWith(313);
  });
});

export {};
