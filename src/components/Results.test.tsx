import { shallow } from 'enzyme';

import { Results } from './Results';

describe('Results component', () => {
  it('should render', () => {
    const wrapper = shallow(
      <Results
        consumptionA={5}
        consumptionB={10}
        travelTimeA={10}
        travelTimeB={20}
      />
    );

    expect(wrapper.text()).toContain(
      'Normaalinopeudella matka-aika olisi 10.0 minuuttia'
    );
    expect(wrapper.text()).toContain('vertailunopeudella 20.0 minuuttia');
    expect(wrapper.text()).toContain('Ajamalla hitaammin');
    expect(wrapper.text()).toContain(
      'Normaalinopeudella polttoaineen kulutus olisi 5.0 litraa'
    );
    expect(wrapper.text()).toContain('vertailunopeudella 10.0 litraa');
    expect(wrapper.text()).toContain('euroa enemmän kuin normaalinopeutta');
  });
});

export {};
