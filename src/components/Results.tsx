import { config } from '../App-config';

type ResultsProps = {
  consumptionA: number;
  consumptionB: number;
  travelTimeA: number;
  travelTimeB: number;
};

const Results = ({
  consumptionA,
  consumptionB,
  travelTimeA,
  travelTimeB,
}: ResultsProps): JSX.Element => {
  const formatTravelTime = (travelTime: number): string => {
    return `${travelTime.toFixed(1)} minuuttia`;
  };

  const travelTimeDiff = (travelTimeA: number, travelTimeB: number): string => {
    const diff = Math.abs(travelTimeB - travelTimeA);

    if (travelTimeB < travelTimeA) {
      return `Ajamalla nopeammin säästäisit kokonaismatka-ajassa
        ${formatTravelTime(diff)}`;
    } else {
      return `Ajamalla hitaammin matka-aikasi olisi
        ${formatTravelTime(diff)} pidempi.`;
    }
  };

  const formatConsumption = (consumption: number): string => {
    return `${consumption.toFixed(1)} litraa`;
  };

  const consumptionDiff = (
    consumptionA: number,
    consumptionB: number
  ): string => {
    const diff = Math.abs(consumptionB - consumptionA);
    const diffFuelPrice = (diff * config.fuel.price).toFixed(1);

    if (consumptionB < consumptionA) {
      return `Ajamalla nopeammin säästäisit polttoainetta ${formatConsumption(
        diff
      )}, eli säästäisit ${diffFuelPrice} euroa.`;
    } else {
      return `Ajamalla hitaammin polttoaineen kulutuksesi olisi ${formatConsumption(
        diff
      )} enemmän, jolloin matkasi maksaisi ${diffFuelPrice} euroa enemmän kuin normaalinopeutta ajaessasi.`;
    }
  };

  return (
    <>
      <h3>Matka-aika</h3>

      {travelTimeA !== travelTimeB ? (
        <>
          <p>
            Normaalinopeudella matka-aika olisi {formatTravelTime(travelTimeA)}{' '}
            ja vertailunopeudella {formatTravelTime(travelTimeB)}.{' '}
          </p>
          <p style={{ fontWeight: 'bolder' }}>
            {travelTimeDiff(travelTimeA, travelTimeB)}
          </p>
        </>
      ) : (
        <p>Ei eroa matka-ajassa.</p>
      )}

      <h3>Polttoaineen kulutus</h3>

      {consumptionA !== consumptionB ? (
        <>
          <p>
            Normaalinopeudella polttoaineen kulutus olisi{' '}
            {formatConsumption(consumptionA)} ja vertailunopeudella{' '}
            {formatConsumption(consumptionB)}.{' '}
          </p>
          <p style={{ fontWeight: 'bolder' }}>
            {consumptionDiff(consumptionA, consumptionB)}
          </p>
        </>
      ) : (
        <p>Ei eroa polttoaineen kulutuksessa.</p>
      )}
    </>
  );
};

export { Results };
