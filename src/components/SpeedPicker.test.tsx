import { shallow } from 'enzyme';

import { SpeedPicker } from './SpeedPicker';

describe('SpeedPicker component', () => {
  it('should render an input with given speed', () => {
    const wrapper = shallow(<SpeedPicker speed={100} setSpeed={jest.fn()} />);

    expect(wrapper.find('span').text()).toContain('100');
    expect(wrapper.find('input.slider').exists()).toEqual(true);
  });

  it('should should call setSpeed', () => {
    const mockRangeChange = jest.fn();

    const wrapper = shallow(
      <SpeedPicker speed={100} setSpeed={mockRangeChange} />
    );

    expect(mockRangeChange).not.toHaveBeenCalled();

    wrapper.find('input').simulate('change', { target: { value: 313 } });

    expect(mockRangeChange).toHaveBeenCalledWith(313);
  });
});

export {};
