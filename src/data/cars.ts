const cars = [
  {
    name: 'A',
    baseConsumption: 3,
    icon: 'station-wagon',
  },
  {
    name: 'B',
    baseConsumption: 3.5,
    icon: 'suv',
  },
  {
    name: 'C',
    baseConsumption: 4,
    icon: 'sports',
  },
];

export { cars };
