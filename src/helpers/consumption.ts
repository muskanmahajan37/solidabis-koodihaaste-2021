const SLOPE = 1.009;

/**
 * Returns total consumption in litres
 *
 * @param baseConsumption   consumption l/100km @ 1km/h
 * @param speed             average speed
 * @param distance          total distance
 */
const calculateConsumption = (
  baseConsumption: number,
  speed: number,
  distance: number
): number => {
  return (baseConsumption * Math.pow(SLOPE, speed - 1) * distance) / 100;
};

export { calculateConsumption };
