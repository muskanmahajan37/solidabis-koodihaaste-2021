export interface ICar {
  name: string;
  baseConsumption: number;
  icon: string;
}
